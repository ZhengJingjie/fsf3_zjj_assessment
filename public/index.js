/**
 * Created by Jingjie on 21/10/16.
 */

(function() {
    var RegApp = angular.module("RegApp", []);

    var RegCtrl = function ($http) {
        var regCtrl = this;

        regCtrl.regParticular = {
            name: "",
            dob: "",
            gender: "",
            email: "",
            contact: "",
            country: "",
            address: "",
            password: ""
        };

        regCtrl.regParticulars = [];
        regCtrl.successMessage = "Thank you";
        regCtrl.success;

        regCtrl.ageCal = function () {
            var ageDifMs = Date.now() - regCtrl.dob.getTime();
            var ageDate = new Date(ageDifMs);
            regCtrl.age = Math.abs(ageDate.getUTCFullYear() - 1970);
            return;
        };

        regCtrl.register = function() {
            regCtrl.regParticulars.push(regCtrl.regParticular);

            $http
                .post("api/users", regCtrl.regParticular)
                .then(function(){
                    regCtrl.success = true;
                })
                .catch(function(){
                    regCtrl.success = false;
                });
        };
    };

    RegApp.controller("RegCtrl", ["$http", RegCtrl]);
})();