/**
 * Created by Jingjie on 21/10/16.
 */

const path = require("path");
const docroot = path.join(__dirname, "..");

const express = require("express");
const app = express();

app.set("port", process.argv[2] || process.env.APP_PORT || 3000);

app.use(express.static(path.join(docroot, "public")));

app.use(express.static(path.join(docroot, "bower_components")));

app.post("api/users", function (req, res){
    res.json(req.body);
    console.log(req.body);
});

app.listen(app.get("port"), function(){
    console.info("Application started on port %d", app.get("port"));
});